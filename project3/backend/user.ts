
 
 interface Users{
    id:number |string
    name:string
    room:string 
}
// interface UsersDataset{
//     users:Users[] |undefined
// }

 

const users: Users[]=[];

const addUser=({id,name,room}:Users)=>{
 
    // name =name.trim().toLowerCase()
    // room =room.trim().toLowerCase() 


    const existingUser= users.find((user)=>user.room && user.name ===name)

    if(!name || ! room ) return {error:'Usename and room are require'}
    if(existingUser) return {error:'Userma,e is taken'}

    const user ={id,name,room}

    users.push(user);
    return{user}

}


 const removeUser =(id: number)=>{
     const index =users.findIndex((user)=>user.id===id);
     
   if(index !== -1){
    return users.splice(index,1)[0]
   }
   return users.splice(index)
 }

const getUser =(id:number)=>users.find((user)=>user.id=== id)
const getUsersInRoom =(room:string)=>users.filter((user)=>user.room===room)


export {addUser,getUser,getUsersInRoom,removeUser}


