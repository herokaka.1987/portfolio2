import knex from 'Knex';

export class CategoryService{
    constructor(private knex:knex){}

    async getPostByCategory(category: string) {
        if (category == null) {
            const allPost = await this.knex.raw(/*SQL*/`SELECT * FROM post`)
        return allPost.rows
        } else {
            const getPostCate = await this.knex.raw(/*SQL*/`SELECT * FROM post WHERE category=?`, [category])
            return getPostCate.rows
        }
    }
}