import express from 'express';
import { Request, Response } from 'express';
import { CategoryService } from './CategoryService'

export class CategoryRouter {
    constructor(private categoryService: CategoryService) {}
    router() {
        const router = express.Router();
        router.post('/category', this.selectPostByCate)
        return router
    }
    selectPostByCate = async (req: Request, res: Response) => {
        const GetPost = this.categoryService.getPostByCategory(req.body.value)
        return GetPost;
    }
}