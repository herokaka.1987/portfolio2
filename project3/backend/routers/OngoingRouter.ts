import express from 'express';
import { Request, Response } from 'express';
import { OngoingService } from '../services/OngoingService'

export class OngoingRouter {
    constructor(private ongoingService: OngoingService) { }
    router() {
        const router = express.Router();
        router.get('/ongoing', this.OngoingEvent)
        router.get('/history', this.GetHistory)
        return router
    }

    OngoingEvent = async (req: Request, res: Response) => {
        
        //@ts-ignore
        const getPostId = await this.ongoingService.getAttender(req.user);

        if (getPostId[0] === undefined) {
            res.status(401).json({ message: '未有進行中的活動！' })
            return;
        } else {
        
            const getOngoingPost = await this.ongoingService.getOngoingEvent(getPostId[getPostId.length -1].post_id);
            
            res.status(200).json(getOngoingPost);

        }
    }

    GetHistory = async (req: Request, res: Response) => {

        //@ts-ignore
        const getPostId = await this.ongoingService.getAttender(req.user);
        console.log(getPostId)
        if (getPostId[0] === undefined) {
            res.status(401).json({ message: '未曾參與過任何活動！' })
            return;
        } else {
            const history = await this.ongoingService.getOngoingEvent(getPostId[0].post_id);
            if (history === undefined) {
                res.status(401).json({ message: '未曾參與過任何活動！' })
                return;
            } else {
                console.log(history)
                return history

            }
        }
    }
}