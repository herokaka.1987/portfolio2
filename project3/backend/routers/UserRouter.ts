import express from "express"
import {Request,Response} from 'express'
import {UserService} from "../services/UserService"
import jwtSimple from 'jwt-simple';
import jwt from '../jwt'
import {isLoggedIn} from "../guard"

export class UserRouter {
    constructor(private userService:UserService){}
    router() {
        const router = express.Router()
        const isLoggedInGuard = isLoggedIn(this.userService)
        router.post('/loginSucceed',this.checkUser)
        router.get('/getUserInfo',isLoggedInGuard,this.userInfo)
        return router
    }

    checkUser =async (req:Request,res:Response)=> {
        
        try {
            if (! req.body.email) {
                res.status(401).json({message:'Wrong Username/Password'});
                return
            }

            const userEmail: string = await this.userService.getUser(req.body.email)
            console.log('userRouter Running')
            
            if(!userEmail) {
                this.userService.createUser(req.body.first_name,req.body.email,req.body.icon)
                res.json({message:'Create account succeed!'})
                return
            }

            const payload = {
                email:userEmail
            }

            const token = jwtSimple.encode(payload,jwt.jwtSecret);

            res.json({
                token:token
            })
        }catch(e) {
            console.log(e)
            res.status(500).json({message:e.toString()})
        }
    }

   userInfo = async (req:Request,res:Response)=> {
        if (req.user === undefined) {
            return
        }else {
        //@ts-ignore
        const info = await this.userService.getUserInfo(req.user)
        res.status(200).json(info)
        }
   }
}