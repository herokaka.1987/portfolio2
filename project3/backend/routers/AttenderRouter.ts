import express from 'express';
import { Request, Response } from 'express';
import { AttenderService } from '../services/AttenderService'

export class AttenderRouter {
    constructor(private attenderService: AttenderService) { }
    router() {
        const router = express.Router();
        router.post('/users', this.Attender)
        return router
    }

    Attender = async (req: Request, res: Response) => {
        const onwerEmail = await this.attenderService.checkOnweremail(req.body.post_id)
        const maxPpl = await this.attenderService.checkPostPpl(req.body.post_id);
        const eventPpl = await this.attenderService.checkEventppl(req.body.post_id);
        const checkDuplicated = await this.attenderService.duplicatedppl(req.body.post_id, req.body.attender_email)

        if (checkDuplicated.rows[0] != null) {
            return res.json({ statusCode: 406 })
        } else if(onwerEmail.rows[0].useremail === req.body.attender_email) {
            return res.json({ statusCode: 401 })
        } else if (eventPpl.rows[0].count >= maxPpl.rows[0].ppl) {
            return res.json({
                success: false
            });
        } else {
            await this.attenderService.postwithAttender(req.body.post_id, req.body.attender_email)
            return res.json({
                success: true
            })
        }
    }
}