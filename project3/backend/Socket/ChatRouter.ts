import express from "express";
import { Request, Response } from "express";
import { ChatService } from './ChatService'

import socketIO from 'socket.io';


export class ChatRouter {
    constructor(private chatService: ChatService, private io: socketIO.Server) { }

    router() {
        const router = express.Router();
        router.post('/questions',this.PostChat)
        router.get('/questions',this.getChat)

        return router
    }
    getChat = async (req: Request, res: Response) => {
        try {
            const questions = await this.chatService.getChat()
            res.json(questions.filter((questions: { posting_id: number; }) => questions.posting_id === parseInt(req.query.posting_id + "")));

        } catch (e) {
            console.error(e);
            res.status(500).json({ success: false });
        }

    }

    PostChat= async(req:Request,res:Response)=>{
        try{

      await this.chatService.createChat(req.body.question,req.body.posting_id,req.body.username,req.body.useremail)
      console.log(req.body.posting_id)
      this.io.to(`posting:${req.body.posting_id}`).emit('new_question', req.body.content);

      res.json({success: true});

        }catch(e){
            console.error(e);
            res.status(500).json({ success: false });
        }
    }


}