import express from 'express';
import { Request, Response } from 'express';
import { OngoingService } from './OngoingService'

export class OngoingRouter {
    constructor(private ongoingService: OngoingService) { }
    router() {
        const router = express.Router();
        router.get('/ongoing', this.OngoingEvent)
        router.get('/history', this.GetHistory)
        return router
    }

    OngoingEvent = async (req: Request, res: Response) => {
        console.log(`year!!!!!${req.user}`)
        if (req.user?.email === undefined) {
            return
        } else {
            const getPostId = await this.ongoingService.getAttender(req.user.email);
            console.log(getPostId)
            if (getPostId[0] === undefined) {
                res.status(401).json({ message: '未有進行中的活動！' })
                return;
            } else {
                console.log("test")
                const getOngoingPost = await this.ongoingService.getOngoingEvent(getPostId[0].post_id);
                console.log(getOngoingPost)
                res.status(200).json(getOngoingPost);

            }
        }
    }

    GetHistory = async (req: Request, res: Response) => {
        console.log('history!!')
        //@ts-ignore
        const getPostId = await this.ongoingService.getAttender(req.user);
        console.log(getPostId);
        if (getPostId[0] === undefined) {
            res.status(401).json({ message: '未曾與過任何活動！' })
            return;
        } else {
            const history = await this.ongoingService.getOngoingEvent(getPostId[0].post_id);
            console.log(history)
            return history
        }
    }
}