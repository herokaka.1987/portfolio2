import knex from 'knex';

export class OngoingService {
    constructor(private knex:knex){}

    async getAttender(attender_email: string) {
        const check = await this.knex.raw(/*SQL*/`SELECT post_id FROM attender WHERE attender_email=?`, [attender_email])
        return check.rows
    }

    async getOngoingEvent(post_id:number){
        const OngoingEvent = (await this.knex.raw(/* sql */ `SELECT * FROM post WHERE endtime >= CURRENT_TIMESTAMP(2) AND id=?`,[post_id]))
        return OngoingEvent.rows[0]
    }
}