import knex from 'knex';

export class AttenderService {
    constructor(private knex:knex){}

    async postwithAttender(post_id: number, attender_email: string) {
        const attender = await this.knex.raw(/*SQL*/ `INSERT INTO attender (post_id, attender_email) VALUES (?,?) RETURNING id`, [post_id, attender_email])
        return attender.rows
    }

    async checkPostPpl(id: number) {
        const result = await this.knex.raw(/*SQL*/ `SELECT ppl from post WHERE id=? `,[id])
        return result;
    }

    async checkOnweremail(id: number) {
        const onwerEmail = await this.knex.raw(/*SQL*/ `SELECT useremail FROM post WHERE id = ?`, [id])
        return onwerEmail;
    }

    async checkEventppl(post_id: number) {
        const ppl = await this.knex.raw(/*SQL*/`SELECT COUNT(*) FROM attender WHERE post_id=?`,[post_id])
        return ppl;
    }

    async duplicatedppl(post_id: number, attender_email: string) {
        const check = await this.knex.raw(/*SQL*/`SELECT * FROM attender WHERE post_id=? AND attender_email=?`, [post_id, attender_email])
        return check;
    }
}