import knex from 'knex'

export interface User {
    email:string;
}

export class UserService {
    constructor(private knex:knex) {}
    async getUser(email:string) {
        const result = await this.knex.raw(/*SQL*/`SELECT email FROM users WHERE email=?`,[email])
        if(result.rows[0] === undefined) {
            return
        }else {
            return result.rows[0].email
        }
    }

    async createUser(first_name:string,email:string,icon:string) {
        const result = await this.knex.raw(/*SQL*/`INSERT INTO users (username,email,likes,icon) VALUES (?,?,?,?) RETURNING id`,[first_name,email,0,icon])
        return result.rows[0].id
    }


    async getUserInfo(email:string) {
        const info = await this.knex.raw(/*SQL*/`SELECT * FROM users WHERE email = ?`,[email])
        return info.rows[0]
    }
}