import {Bearer} from 'permit';
import express from 'express';
import {Request,Response} from 'express'
import jwtSimple from 'jwt-simple';
import jwt from './jwt';
import {UserService,User} from './services/UserService';


const permit = new Bearer({
    query:'access_token'
})

export function isLoggedIn(userService:UserService) {
    
    return async (
        req: Request,
        res: Response,
        next: express.NextFunction
    ) => {
        try{
            const token = permit.check(req);
            console.log(`guard ${token} ` )
            // console.log(req)
            if(!token) {
                
                return res.status(401).json({message:'Permission Denied 1'})
            }
            const payload = jwtSimple.decode(token,jwt.jwtSecret);
            const user:User = await userService.getUser(payload.email)
            if(user) {
                req.user = user
                
                return next();
            }else {
                return res.status(401).json({message:'Permission Denied 2'});
            }
        }catch(e) {
            return res.status(401).json({message:'Permisson Denied 3'})
        }
    }
}