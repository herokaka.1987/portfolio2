import express from 'express';
import { Request, Response } from 'express';
import { FormService } from './FormService';
import path from 'path'
import multer from 'multer'


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, 'uploads'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const upload = multer({ storage: storage })

export class FormRouter {
    constructor(private formService: FormService) { }
    router() {
        const router = express.Router();
        router.post('/form', upload.single('image'), this.sendPost)
        router.get('/form', this.getPost)
        return router
    }
    sendPost = async (req: Request, res: Response) => {
        try {
            console.log("111222333")
            console.log(req.file.filename);
            await this.formService.createPost(req.body.title, req.body.host, req.body.district, req.body.category, req.body.address, req.body.ppl, req.body.time, req.body.endtime, req.body.lat, req.body.lng, req.body.price, req.body.info, req.body.useremail, req.file.filename || 'No Photo')
            res.json({ success: true });
        } catch (e) {
            console.error(e)
            res.status(500).json({ status: false })
        }
        
    }
    
    getPost = async (req: Request, res: Response) => {
        try {
            const posts = await this.formService.getPost();
            res.json(posts);
        } catch (e) {
            console.error(e);
            res.status(500).json({ success: false });
        }
    }
}