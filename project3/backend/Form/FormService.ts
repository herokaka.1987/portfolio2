import knex from 'knex';

export class FormService {
    constructor(private knex:knex){}
    async createPost(title: string,
        host: string,
        district: string,
        category: string,
        address: string,
        ppl: number,
        time: Date,
        endtime: Date,
        lat: string,
        lng: string,
        price: number,
        info: string,
        useremail: string,
        image: string) {

        const data = await this.knex.raw(/* sql */ `INSERT INTO post 
        (title, host, district, category, address, ppl, time, endtime, lat, lng, price, info, useremail, image) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?) RETURNING id`
         ,[title, host, district, category, address, ppl, new Date(time), new Date(endtime), lat, lng, price, info, useremail, image]);
         return data.rows
    }

    async getPost(){
        const DB_form = (await this.knex.raw(/* sql */ `SELECT * FROM post WHERE endtime >= CURRENT_TIMESTAMP(2)`))
        return DB_form.rows
    }
}