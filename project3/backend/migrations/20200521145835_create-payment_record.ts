import Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    await knex.schema.createTable('payment_record', table => {
        table.increments();
        table.integer('users_id').unsigned;
        table.foreign('users_id').references('users.id');
        table.integer('post_id').unsigned;
        table.foreign('post_id').references('post.id');
        table.timestamps(false, true);
    }) 
    
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('payment_record')
}

