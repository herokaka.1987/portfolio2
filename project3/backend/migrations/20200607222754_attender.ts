import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    await knex.schema.createTable('attender', table => {
        table.increments();
        table.integer('post_id');
        table.foreign('post_id').references('post.id');
        table.string('attender_email');
        table.foreign('attender_email').references('users.email');
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('attender')
}

