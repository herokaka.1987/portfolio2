import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    
    await knex.schema.createTable('history', table => {
        table.increments();
        table.integer('post_id').unsigned;
        table.foreign('post_id').references('post.id');
        table.integer('users_id').unsigned;
        table.foreign('users_id').references('users.id')
    })
    
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('history')
}

