import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
        await knex.schema.createTable('post',table=> {
            table.increments();
            table.string('title');
            table.string('host');
            table.string('useremail');
            table.foreign('useremail').references('users.email');
            table.string('district');
            table.string('category');
            table.string('address');
            table.integer('ppl').notNullable;
            table.dateTime('time').notNullable;
            table.dateTime('endtime').notNullable;
            table.string('lat');
            table.string('lng');
            table.decimal('price', null);
            table.text('info');
            table.text('image').nullable;
            table.timestamps(false,true);
        })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('post')
}

