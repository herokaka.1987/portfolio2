import * as Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.alterTable('comment', table => {
        table.integer('posting_id').unsigned();
        table.foreign('posting_id').references('post.id')
    });
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.alterTable('comment', table => {
        table.dropForeign(['posting_id'])
        table.dropColumn('posting_id');
    });
}

