import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    await knex.schema.createTable('comment', table => {
        table.increments();
        table.text('content');
        table.string('useremail');
        table.foreign('useremail').references('users.email');
        table.string('username');

    })
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('comment')
}

