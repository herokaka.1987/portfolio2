import Knex from "knex";


export async function up(knex: Knex): Promise<any> {
    // const hasTable = knex.schema.hasTable('photo')
    // if(!hasTable) {
        await knex.schema.createTable('photo',table=> {
            table.increments();
            table.integer('post_id').unsigned;
            table.foreign('post_id').references('post.id');
            table.text('photo_name');
        })
    // }
}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('photo')
}

