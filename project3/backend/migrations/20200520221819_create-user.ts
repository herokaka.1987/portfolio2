import Knex from "knex";


export async function up(knex: Knex): Promise<any> {

    await knex.schema.createTable('users', table => {
        table.increments();
        table.string('email').unique();
        table.string('username');
        table.integer('likes');
        table.string('icon');
        table.timestamps(false, true);
    })

}


export async function down(knex: Knex): Promise<any> {
    await knex.schema.dropTable('users')
}

