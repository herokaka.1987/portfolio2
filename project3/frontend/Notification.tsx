import React, { useEffect } from 'react'
import PushNotification from 'react-native-push-notification'
const RemotePushController = () => {
  useEffect(() => {
    // FirebaseMessaging.AuthorizationStatus.DENIED
    // return
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function(token) {
        // console.log('TOKEN:', token)
      },
// (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
        PushNotification.localNotification({
            //@ts-ignore
            title: notification.title, // (optional)
            message: notification.message as string, // (required)
            playSound:true,
            soundName:'default',
          });
        console.log('REMOTE NOTIFICATION ==>', notification)
// process the notification here
      },
// IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
            alert: true,
            badge: true,
            sound: true,
        },
      // Android only: GCM or FCM Sender ID
      senderID: '256218572662',
      popInitialNotification: true,
      requestPermissions: true
    })
  }, [])
return null
}
export default RemotePushController