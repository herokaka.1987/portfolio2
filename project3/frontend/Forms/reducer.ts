import { FormsActions } from './action'

export interface FormDetail {
    title: string;
    host: string;
    district: string;
    category: string;
    address: string;
    ppl: number;
    time: string;
    endtime: string;
    lat: number;
    lng: number;
    price: number;
    info: string; 
    image: string;
    useremail: string;
    id: number
}

export interface FormDetailState {
    forms : FormDetail[],
    sumitting: boolean,
}

const initialState: FormDetailState = {
    forms : [],
    sumitting: false,
}

export const formReducer = (oldState: FormDetailState = initialState, action: FormsActions): FormDetailState => {
    switch (action.type) {
        case "@@FORMS/SUBMITTING":
            return {
                ...oldState,
                sumitting: action.isSubmitting
            }
            
        // case "@@FORMS/EDIT_MEMO":
        //     const newForms = oldState.forms.slice();
        //     return {
        //         ...oldState,
        //         forms: newForms
        //     }
        case "@@FORMS/LOADED_FORMS": 
            return {
                ...oldState,
                forms: action.forms,
            }
            default: break;
    }
    return oldState;
}