/* eslint-disable prettier/prettier */
import { loadForm, setSubmitting } from './action';
import { ThunkDispatch, RootState } from '../store';
import config from '../config';
// import {useSelector} from 'react-redux'

//Thunk Action

interface form {
    title: string;
    host: string;
    district: string;
    category: string;
    address: string;
    ppl: number;
    time: string;
    endtime: string;
    lat: string;
    lng: string;
    price: number;
    info: string;
    image: {name: string};
    useremail: string;
    id: number;
}


export function fetchForm() {
    return async (dispatch: ThunkDispatch,getJwtToken:()=>RootState) => {
        const jwt_Token = getJwtToken().auth.token

<<<<<<< HEAD
        const fetchForm = await fetch('https://hphour.xyz/post/form', {
=======
        const fetchForm = await fetch(`http://localhost:8080/post/form`, {
>>>>>>> 09772d3b1e0b0455f470a7c2f650a595faff7480
            credentials: "include",
            headers:{
                
                'Authorization': `Bearer ${jwt_Token}`
            },
        });
        
        const forms = await fetchForm.json();
        // console.log(forms);
        dispatch(loadForm(forms));
    }
}

export function createForm(data: form) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const jwt_Token = getState().auth.token
        if (getState().form.sumitting) {
            console.log('asdsasda')
            return;
        }

        dispatch(setSubmitting(true))
        setTimeout(() => {
            dispatch(setSubmitting(false))
        }, 50000);
        console.log(data.useremail)
        const formData = new FormData();

        formData.append('title', data.title);
        formData.append('host', data.host);
        formData.append('district', data.district);
        formData.append('category', data.category);
        formData.append('address', data.address);
        formData.append('ppl', data.ppl);
        formData.append('time', data.time);
        formData.append('endtime', data.endtime);
        formData.append('lat', data.lat);
        formData.append('lng', data.lng);
        formData.append('price', data.price);
        formData.append('info', data.info);
        formData.append('useremail', data.useremail);
        if (data.image) {
            formData.append('image', data.image);
        } else {
            formData.append('image', 'No Photo');
        }
        if (formData !== null) {
            await fetch(`https://hphour.xyz/post/form/`, {
                method: 'POST',
                credentials: 'same-origin',
                mode: 'same-origin',
                headers: {
                    'Authorization': `Bearer ${jwt_Token}`
                },
                body: formData

            }).then(response => {
                console.log("Form uploaded!")
            }).catch(err => {
                console.log(err);
            })

            console.log(formData);
            dispatch(fetchForm());
        }
    }
}
