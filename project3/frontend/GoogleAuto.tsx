import React from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const GooglePlacesInput = () => {
    return (
        //@ts-ignore
        <GooglePlacesAutocomplete
        fetchDetails={true}
            placeholder='Search'
            onPress={(data, details = null) => {
                // 'details' is provided when fetchDetails = true
                console.log(data, details?.geometry.location);
            }}
            query={{
                key: 'AIzaSyBKSY4eq-V33vPrh04JsfR38bq4IOs-LOA',
                language: 'en',
            }}
        />
    );
};

export default GooglePlacesInput;