import React, { useEffect } from "react"
import {View,ImageBackground,Text} from "react-native"
import styles from "../styleFolder/FirstPage.style"
import Googlelogin from "../login/GoogleLogin"
import FbLogin from "../login/FbLogin"
import { useDispatch } from "react-redux"
import { setJwtToken } from "../login/authAction"
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from "react-native-router-flux"


export default function FirstPageS() {
    const dispatch = useDispatch();

    useEffect(()=> {
        try {
            const initData = async () => {
                try {
                    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')
                    console.log(jwt_Token)
                    if (jwt_Token == null) {
                        Actions.replace('firstPage')
                    } else {
                        // console.log('hihihihi')
                        // await fetch('http://192.168.1.20:8080/decode',{
                        //     method:'POST',
                        //     headers:{
                        //         'Content-Type':'application/json'
                        //     },
                        //     body:JSON.stringify({token:jwt_Token})
                        // })
                        console.log("set token");
                        dispatch(setJwtToken(jwt_Token))
                        Actions.replace('secondPage')
                    } 

                } catch (e) {
                    console.log(e)
                }
            }
            initData()
        } catch {}

    },[dispatch])

    return(
        <View style={styles.container}>
            <ImageBackground source={require('../image/background.jpg')} style={styles.background}>
                <View style={styles.appName}>
                    <Text style={styles.textTitle}>Happy Hour</Text>
                </View>

                <View style={styles.button} >
                    <FbLogin />
                    <Googlelogin />
                </View>
            </ImageBackground>
        </View>
    )
}