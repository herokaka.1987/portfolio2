import React ,{Component} from "react"
import {Header,Left, Right, Icon,Body,Title,View,Button} from "native-base"
import GoogleMap from "../googleMap/GoogleMap"

export default class HomeScreen extends Component {
    
  render() {
    return(
        <View>
            <Header>
                <Left>
                    <Button
                        transparent
                        // @ts-ignore
                        onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                    {/* <Icon style={{color:'white'}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/> */}
                </Left>
                <Body>
                    <Title>按地圖查找</Title>
                </Body>
                <Right/>

                {/* <Right>
                    <Button
                        transparent
                        >
                        <Icon name="book"/>
                    </Button>
                </Right> */}
            </Header>
            <GoogleMap/>
        </View>
    )
  }
}