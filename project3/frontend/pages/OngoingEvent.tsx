import React, { useEffect, useState } from 'react'
import {Image,ScrollView,SafeAreaView,Alert} from "react-native"
import {View,Button,Text, Header, Left, Icon, Title, Body, Right, Content, Card, CardItem} from "native-base"
import config from '../config'
import { RootState } from '../store'
import { useSelector } from 'react-redux'

function OngoingEvent(props: { navigation: { openDrawer: () => void; }; }) {

    const [message, setMessage] = useState('');

    const jwt_Token = useSelector((rootState: RootState) => rootState.auth.token)

    const ongoingEvent = async () => {
        const fetchEvent = await fetch(`https://hphour.xyz/test/ongoing`,{
            headers:{
                'Authorization': `Bearer ${jwt_Token}`
            },
        });
        const Events = await fetchEvent.json();

        setMessage(Events);
        return Events;
    }
    
    useEffect(() => {
        ongoingEvent();
    },[])


    return(
        <View>
            <Header>
                <Left>
                    <Button
                        transparent
                        // @ts-ignore
                        onPress={() => props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                    {/* <Icon style={{color:'white'}} name="menu" onPress={()=>this.props.navigation.openDrawer()}/> */}
                </Left>
                <Body>
                    <Title>進行中的活動</Title>
                </Body>
                <Right />
            </Header>
            
            {/* <Text style={{flex:1 ,justifyContent:"center", position:"absolute", left:"35%", top:"400%", }}>{message.message}</Text> */}
            {message.address == undefined ? <Text>{message.message}</Text> : 

            <ScrollView>
<<<<<<< HEAD
                <Image style={{width:'100%', height:200,marginTop:15}} source={{uri: `https://hphour.xyz/post/form/${message.image}`}}/>
=======
                <Image style={{width:'100%', height:200,marginTop:15}} source={{uri: `http://localhost:8080/post/form/${message.image}`}}/>
>>>>>>> 09772d3b1e0b0455f470a7c2f650a595faff7480

                    <View>
                        <View style={{ marginBottom: 0 }}>
                            <Content padder>
                                <Card>
                                    <CardItem header bordered>
                                        <Text style={{ fontSize: 25, margin: "1%" }}>主題:{message.title}</Text>
                                    </CardItem>
                                    <CardItem bordered>
                                        <Body>
                                            <Text style={{ color: 'rgb(65, 59, 142)', margin: "1%" }}>發起人:{message.host}</Text>
                                            <Text style={{ fontSize: 15, margin: "1%" }}>開始時間:{new Date(message.time).toLocaleTimeString()}</Text>
                                            <Text style={{ fontSize: 15, margin: "1%" }}>完結時間:{new Date(message.endtime).toLocaleTimeString()}</Text>
                                            <Text style={{ fontSize: 15, margin: "1%" }}>活動簡介:{message.info}</Text>
                                        </Body>
                                    </CardItem>
                                    <CardItem footer bordered>
                                        <Text style={{ fontSize: 25, margin: "1%" ,color:'red'}}>記得準時出席啦！！！！</Text>
                                    </CardItem>
                                </Card>
                            </Content>
                        </View>
                    </View>

                {/* <View style={{alignItems:'center'}}>
                    <Text style={{ fontSize: 25, margin: "1%" }}>主題:{message.title}</Text>
                    <Text style={{ color: 'rgb(65, 59, 142)', margin: "1%" }}>發起人:{message.host}</Text>
                    <Text style={{ fontSize: 15, margin: "1%" }}>開始時間:{new Date(message.time).toLocaleTimeString()}</Text>
                    <Text style={{ fontSize: 15, margin: "1%" }}>完結時間:{new Date(message.endtime).toLocaleTimeString()}</Text>
                    <Text style={{ fontSize: 15, margin: "1%" }}>活動簡介:{message.info}</Text>
                </View> */}
            </ScrollView>}
        </View>
    )
}

export default OngoingEvent;
