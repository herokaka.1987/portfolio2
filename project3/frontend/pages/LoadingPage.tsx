import React,{useEffect} from "react"
import AsyncStorage from "@react-native-community/async-storage"
import { useDispatch } from "react-redux"
import { checkLoginFB, checkLoginGG } from "../login/action"
import {setJwtToken} from "../login/authAction"
import {Actions} from "react-native-router-flux"
import {View,Text, ImageBackground} from "react-native"
import styles from "../styleFolder/FirstPage.style"

export default function LoadingPage() {

    const dispatch = useDispatch()

    useEffect(()=> {
        try {
            const initData = async () => {
                try {
                    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')
                    
                    if (jwt_Token == null) {

                        console.log('null token')

                        Actions.replace('firstPage')
                    } else {
                        console.log('has token')
                        const res = await fetch('https://hphour.xyz/getUserInfo',{
                            headers:{
                                'Authorization': `Bearer ${jwt_Token}`,
                                'Content-Type':'application/json'
                            }
                        })
                        const result = await res.json()
                        
                        if (result.username == null) {
                            Actions.replace('firstPage')
                            return;
                        }
                        
                        try {

                            dispatch(checkLoginFB(result.username,result.icon,result.email))
                            dispatch(checkLoginGG(result.username,result.icon,result.email))
                        }catch (e) {
                            console.log(e)
                        }
                        
                        dispatch(setJwtToken(jwt_Token))
                        Actions.replace('secondPage')
                    } 

                } catch (e) {
                    console.log(e)
                }
            }
            initData()
        } catch {}

    },[dispatch])
    
    return(
        <View style={styles.container}>
            <ImageBackground source={require('../image/background.jpg')} style={styles.background}>
                <View style={styles.appName}>
                    <Text style={styles.textTitle}>Happy Hour</Text>
                </View>
            </ImageBackground>
        </View>
    )
}