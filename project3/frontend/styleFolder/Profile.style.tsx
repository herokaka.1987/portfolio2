import {StyleSheet} from "react-native"

export default StyleSheet.create ({
    cardHeader: {
        width:'100%',
        height:'50%',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
    },

    icon: {
        width:150,
        height:150,
        borderRadius:50,
        backgroundColor:'red',
        justifyContent:'center',
        alignItems:'center'
    }
})