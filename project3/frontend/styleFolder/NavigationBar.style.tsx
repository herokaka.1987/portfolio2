import {StyleSheet} from "react-native"

export default StyleSheet.create({
    safeareaview: {
        flex:1
    },
    view: {
        height:150,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'white',
    },

    image: {
        width:100,
        height:100,
        borderRadius:50,
    },

    text: {
        fontFamily: 'Pacifico-Regular'
    },

    logout: {
        color:'red'
    }
})