import {StyleSheet} from "react-native"

export default StyleSheet.create({
    container:{
        flex:6,
        flexDirection: 'column',
    },

    background:{
        flex:6,
        resizeMode:'cover',
    },

    appName: {
        flex:3,
        alignItems:'center',
        justifyContent:'center',
    },

    textTitle: {
        fontSize:50,
        fontFamily:"Pacifico-Regular",
    },

    button:{
        flex:3,
        alignItems:'center',
        justifyContent:'space-around',
        flexDirection:'row',
    },
})