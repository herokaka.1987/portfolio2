package com.frontend;

import android.app.Application;
import android.content.Context;
import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.BV.LinearGradient.LinearGradientPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.th3rdwave.safeareacontext.SafeAreaContextPackage;
import com.reactnativecommunity.rnpermissions.RNPermissionsPackage;
import com.imagepicker.ImagePickerPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.dieam.reactnativepushnotification.ReactNativePushNotificationPackage;
import com.reactcommunity.rndatetimepicker.RNDateTimePickerPackage;
import com.reactnativecommunity.geolocation.GeolocationPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.facebook.reactnative.androidsdk.FBSDKPackage;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Arrays;
import com.instabug.reactlibrary.RNInstabugReactnativePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;  // <--- import

import com.airbnb.android.react.maps.MapsPackage;     //googleMap
// import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;      // firebase notification
// import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;         // firebase notification


public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost =
      new ReactNativeHost(this) {
        @Override
        public boolean getUseDeveloperSupport() {
          return BuildConfig.DEBUG;
        }

        @Override
        protected List<ReactPackage> getPackages() {

            return Arrays.<ReactPackage>asList(
                    new MainReactPackage(),
            new LinearGradientPackage(),
            new RNCWebViewPackage(),
            new SafeAreaContextPackage(),
            new RNPermissionsPackage(),
            new ImagePickerPackage(),
            new AsyncStoragePackage(),
            new ReactNativePushNotificationPackage(),
            new RNDateTimePickerPackage(),
            // new RNFirebaseMessagingPackage(),         // firebase notification
            // new RNFirebaseNotificationsPackage(),      // firebase notification
            new GeolocationPackage(),
                    new ReanimatedPackage(),
                    new MapsPackage(),                 //googleMap
                    new FBSDKPackage(),
                    new RNGoogleSigninPackage(), // <-- this needs to be in the list
                    new RNGestureHandlerPackage(),
                    new RNInstabugReactnativePackage.Builder("YOUR_APP_TOKEN", MainApplication.this)
                            .setInvocationEvent("shake", "button", "screenshot")
                            .setPrimaryColor("#1D82DC")
                            .setFloatingEdge("left")
                            .setFloatingButtonOffsetFromTop(250)
                            .build());
          //@SuppressWarnings("UnnecessaryLocalVariable")
          //List<ReactPackage> packages = new PackageList(this).getPackages();
          // Packages that cannot be autolinked yet can be added manually here, for example:
          // packages.add(new MyReactNativePackage());
          //return packages;
        }

        @Override
        protected String getJSMainModuleName() {
          return "index";
        }
      };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    new RNInstabugReactnativePackage
             .Builder("APP_TOKEN", MainApplication.this)
             .setInvocationEvent("shake")
             .setPrimaryColor("#1D82DC")
             .setFloatingEdge("left")
             .setFloatingButtonOffsetFromTop(250)
             .build();
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    // initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
  }

  /**
   * Loads Flipper in React Native templates. Call this in the onCreate method with something like
   * initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
   *
   * @param context
   * @param reactInstanceManager
   */
  private static void initializeFlipper(
      Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.frontend.ReactNativeFlipper");
        aClass
            .getMethod("initializeFlipper", Context.class, ReactInstanceManager.class)
            .invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
