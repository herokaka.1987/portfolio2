/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useCallback } from 'react';
import { View, Image, Alert, ScrollView, StyleSheet} from 'react-native';
import { RootState } from '../store'
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { Content, Card, CardItem, Text, Body, Button, Item, Input  } from "native-base";
import { mapParticipant } from '../Attender/thunk';
import { Actions } from 'react-native-router-flux';
import { socket } from '../socket';
import { addQuestions, upadateQuestions } from '../question/action';
import { fetchQuestion } from '../question/thunk';
import { Question } from '../question/reducer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import PayPal from './Paypal';
import PushNotification from 'react-native-push-notification';

interface Iprops {
    id: number;
}

function PostDetailSon(props: Iprops) {
    const [status, setStatus] = useState('')
    const userEmail = useSelector((state: RootState) => state.login.email)
    const formsinfo = useSelector((state: RootState) => state.form.forms)
    const id = formsinfo.findIndex(x => x.id === props.item)
    const dispatch = useDispatch();
    console.log(id)
    console.log(formsinfo)
    console.log(status);
    console.log(formsinfo[id].title)

    const push = function getNotification() {
        PushNotification.localNotification({
            title: `${formsinfo[id].title}` as string,
            message: `費用: 港幣${formsinfo[id].price}元, 完結時間:${new Date(formsinfo[id].endtime).toLocaleTimeString()}` as string,
        });
    }

    const getStatus = useCallback((status) => {
        setStatus(status);
    }, [setStatus])

    if (status == 'Complete') {
        push()
        // return setStatus('Pending')
    }

    return (
        <ScrollView>
            <Image style={{ width: '100%', height: 200,marginTop:15}} source={{ uri: `https://hphour.xyz/post/form/${formsinfo[id].image}` }} />
            <View>
                <View style={{marginBottom:0}}>
                    <Content padder>
                        <Card>
                            <CardItem header bordered>
                                <Text style={{ fontSize: 25, margin: "1%" }}>主題: {formsinfo[id].title}</Text>
                            </CardItem>
                            <CardItem bordered>
                                <Body>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>地區: {formsinfo[id].district}</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>詳細地址: {formsinfo[id].address}</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>需要人數: {formsinfo[id].ppl}</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>活動開始時間: {new Date(formsinfo[id].time).toLocaleTimeString()}</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>活動完結時間: {new Date(formsinfo[id].endtime).toLocaleTimeString()}</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>費用: 港幣{formsinfo[id].price}元</Text>
                                    <Text style={{ fontSize: 15, margin: "1%" }}>簡介: {formsinfo[id].info}</Text>
                                </Body>
                            </CardItem>
                            <CardItem footer bordered>
                                <Text style={{margin: "1%" }}>發起人: {formsinfo[id].host}</Text>
                            </CardItem>
                        </Card>
                    </Content>
                </View>
            </View>
            <View style={{ justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row'}}>
                <Button style={{ width: 50, height: 50, borderRadius: 50 }} onPress={() => (Alert.alert('真係參加？無得飛機喎～', '', [{ text: '參加', onPress: async () => { Actions.push('SecondPage'); dispatch(mapParticipant(props.item, userEmail)) } }, { text: '取消' }]))}><Text>參加</Text></Button>
                <PayPal getStatus={setStatus} />
            </View>
        </ScrollView>
    )
}

function CommentBoard(props) {
    const FBinfo = useSelector((state: RootState) => state.login.first_name)
    const userEmail = useSelector((state: RootState) => state.login.email)
    const [profile, setProfile] = useState({});
    const [post, setPost] = useState("")
    console.log(props.item)
    const name = FBinfo?.toString() as string
    console.log(post)
    async function testing() {
        const jwt_Token = await AsyncStorage.getItem('@jwt_Token');
        const res = await fetch('https://hphour.xyz/getUserInfo', {
          headers: {
            Authorization: `Bearer ${jwt_Token}`,
          },
        });
        const loginPackage = await res.json();
    
        setProfile(loginPackage);
      }
    
    const data = {
        question: post,
        posting_id: props.item,
        username: name,
        useremail: userEmail
    }

    const postingId = props.item
    console.log(postingId)
    const dispatch = useDispatch();

    const questionsIds = useSelector((state: RootState) => postingId == null ? undefined : state.questions.questionsByPosting[postingId])
    const questions = useSelector((state: RootState) => questionsIds?.map(id => state.questions.questionsById[id]), shallowEqual)
    const id = questions?.map((question) => question.id)
    console.log(id);
    useEffect(() => {

        testing();
        dispatch(fetchQuestion(postingId))
    }, [postingId, dispatch])

    useEffect(() => {
        socket.emit('join_posting', postingId)
        const newQuestionListener = (question: Question) => {
            dispatch(addQuestions(question))

        };
        socket.on('new_question', newQuestionListener)

        const upadteQuestionListener = (question: Question) => {
            dispatch(upadateQuestions(question))

        };
        socket.on('vote_question', upadteQuestionListener)



        return () => {
            socket.off('new_question', newQuestionListener)
            socket.off('vote_question', upadteQuestionListener)
            socket.emit('leave_posting', postingId)
        }
    }, [postingId, dispatch])
    console.log(questionsIds)
    console.log(questions)

    return (
        <>
            <ScrollView >

                {questions?.map((question) => (
                    <View>
                        <Item regular >
                        <Image source={{uri: `${profile.icon}`}} style={styles.image} />
                            <Text> {question.username}:{question.content}</Text>
                        </Item>
                    </View>
                ))}

                <Input style={{ height: "20%" }} />

                <Item rounded floatingLabel style={{ borderColor: "rgb(65, 59, 142)" }}>
                    <Input
                        value={post}
                        onChangeText={(post: React.SetStateAction<string>) => setPost(post)}
                    />
                </Item>

                <Button
                    style={styles.inputStyle}

                    onPress={() => {
<<<<<<< HEAD
                        fetch(`https://hphour.xyz/Chat/questions`, {
=======
                        fetch(`http://localhost:8080/Chat/questions`, {
>>>>>>> 09772d3b1e0b0455f470a7c2f650a595faff7480
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json'
                            }, body: JSON.stringify(data),
                        }

                        ); console.log(data)
                    }} onPressOut={() => {
                        setPost(post => "");
                    }}>
                    <Text>留言</Text>
                </Button>

            </ScrollView >
        </>
    )
}

const styles = StyleSheet.create({
    root: {
        flex: 2
    },
    inputStyle: {
        bottom: 0,
        marginTop: 30,
        justifyContent: 'center',
    },
    image:{
        width:20,
        height:20,
        borderRadius:50,
    }
})


const Tab = createBottomTabNavigator();

export default function PostDetail(props: Iprops) {

    return (
        <NavigationContainer>
            <Tab.Navigator>
                <Tab.Screen name="活動資料">
                    {() => <PostDetailSon item={props.id} />}
                </Tab.Screen>
                <Tab.Screen name="留言">
                    {() => <CommentBoard item={props.id} />}
                </Tab.Screen>
            </Tab.Navigator>
        </NavigationContainer>
    )
};

