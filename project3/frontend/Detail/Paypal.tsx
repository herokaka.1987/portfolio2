/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React from 'react'
import { View, Text,Modal,StyleSheet} from 'react-native'
import {WebView} from 'react-native-webview';
import { Button} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome'


let jsCode = `
    document.querySelector('.f1').submit()';
`

export default class PayPal extends React.Component {
    state = {
        showModal: false,
        status: "Pending"
    };
  
    handleResponse = (data: { title: string; }) => {
        if (data.title === "success") {
            this.setState({ showModal: false, status: "Complete" });
           
        } else if (data.title === "cancel") {
            this.setState({ showModal: false, status: "Cancelled" });
        } else {
            return;
        }
    };
    render() {
        return (
            <View>
                 
                <Modal
                    visible={this.state.showModal}
                    onRequestClose={() => this.setState({ showModal: false })}
                >
                    <WebView
                        source={{ uri: "https://hphour.xyz" }}
                        onNavigationStateChange={data =>
                            this.handleResponse(data)
                        }
                       
                        injectedJavaScript={jsCode}
                        javaScriptEnabled={true}
                    />
                </Modal>
                <Button style={{justifyContent:'center',width:50,height:50,borderRadius:50}}
                    
                    // style={{ width: 100, height: 100, left:"40%", borderColor:"black"}}
                    onPress={() => this.setState({ showModal: true })}
                >
                    <Icon name="paypal" style={{color:'white',fontSize:20}}/>
                    {/* <Text style={styles.pay}>Pay with Paypal</Text> */}
                </Button>
                {this.props.getStatus(this.state.status)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pay: {
      textAlign:"center",
      color:'white',
    },image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
    }
})