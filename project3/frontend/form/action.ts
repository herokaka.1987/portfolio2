import { FormDetail } from './reducer'

export function loadForm(forms: FormDetail[]) {
    return {
        type: "@@FORMS/LOADED_FORMS" as "@@FORMS/LOADED_FORMS",
        forms,
    };
}

export function setSubmitting(isSubmitting: boolean) {
    return {
        type: "@@FORMS/SUBMITTING" as "@@FORMS/SUBMITTING",
        isSubmitting
    }
}

export type FormsActions = ReturnType<typeof loadForm> | ReturnType<typeof setSubmitting>;