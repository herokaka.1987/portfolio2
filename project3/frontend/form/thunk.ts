/* eslint-disable prettier/prettier */
import { loadForm, setSubmitting } from './action';
import { ThunkDispatch, RootState } from '../store';
import config from '../config';
// import {useSelector} from 'react-redux'

//Thunk Action

interface form {
    title: string;
    host: string;
    district: string;
    category: string;
    address: string;
    ppl: number;
    time: string;
    endtime: string;
    lat: string;
    lng: string;
    price: number;
    info: string;
    image: {uri: string, name: string, type:string};
    useremail: string;
    id: number;
}


export function fetchForm() {
    return async (dispatch: ThunkDispatch,getJwtToken:()=>RootState) => {
        const jwt_Token = getJwtToken().auth.token

        const fetchForm = await fetch(`https://hphour.xyz/post/form`, {
            credentials: "include",
            headers:{
                
                'Authorization': `Bearer ${jwt_Token}`
            },
        });
        
        const forms = await fetchForm.json();
        // console.log(forms);
        dispatch(loadForm(forms));
    }
}

export function createForm(data: form) {
    return async (dispatch: ThunkDispatch, getState: () => RootState) => {
        const jwt_Token = getState().auth.token
        if (getState().form.sumitting) {
            console.log('asdsasda')
            return;
        }

        dispatch(setSubmitting(true))
        setTimeout(() => {
            dispatch(setSubmitting(false))
        }, 50000);
        const formData = new FormData();

        formData.append('title', data.title);
        formData.append('host', data.host);
        formData.append('district', data.district);
        formData.append('category', data.category);
        formData.append('address', data.address);
        formData.append('ppl', data.ppl);
        formData.append('time', data.time);
        formData.append('endtime', data.endtime);
        formData.append('lat', data.lat);
        formData.append('lng', data.lng);
        formData.append('price', data.price);
        formData.append('info', data.info);
        formData.append('useremail', data.useremail);
        if (data.image.name !=null) {
            formData.append('image', data.image);
            console.log(data.image)
        } else {
            formData.append('image', 'No Photo');
        }
        if (formData !== null) {
            await fetch(`${config.BACK_URL}post/form/`, {
                method: 'POST',
                // 
                // credentials: 'same-origin',
                // mode: 'same-origin',
                // 
                headers: {
                    'Authorization': `Bearer ${jwt_Token}`,
                    // 'Content-Type': 'multipart/form-data',
                },
                body: formData,

            }).then(response => {
                console.log("Form uploaded!")
                console.log(response)
            }).catch(err => {
                console.log(err);
            })
            console.log(formData);
            dispatch(fetchForm());
        }
    }
}