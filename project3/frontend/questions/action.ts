import {Question} from './reducer'
import { Posting } from 'container/reducer'


export function  loadQuestions(questions:Question[],postingId:number
    ){
return{
    type:"@@QUESTIONS/LOAD_QUESTIONS" as  "@@QUESTIONS/LOAD_QUESTIONS",
    questions,
    postingId,
  


}
    }


 export function  addQuestions(question:Question
        ){
            return{
                type:"@@QUESTIONS/ADD_QUESTION" as "@@QUESTIONS/ADD_QUESTION",
                question
            }
         
        }


export function  upadateQuestions(question:Question
            ){
            return{
                type:"@@QUESTIONS/UPLOAD_QUESTION" as "@@QUESTIONS/UPLOAD_QUESTION",
                question
            }
            }


export type QuestionActions =ReturnType<typeof loadQuestions>|
            ReturnType<typeof addQuestions>|
            ReturnType<typeof upadateQuestions>