/* eslint-disable prettier/prettier */
import { ThunkDispatch } from '../store';
import { loadQuestions } from './action'
import config from '../config'

export function fetchQuestion(postingId: number) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`https://hphour.xyz/questions?posting_id=${postingId}`)
        console.log(postingId)
        const json = await res.json();
        // console.log(json)
        dispatch(loadQuestions(json, postingId))
    }
}