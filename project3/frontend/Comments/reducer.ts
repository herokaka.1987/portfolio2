import {QuestionActions} from './action'
import  produce from 'immer'
export  interface Question  {
    id:number;
    question:string;
    vote:number;
    posting_id:number
}

export interface QuestionsState{
    questionsById:{
        [questionId:string]:Question,
    },
    questionsByPosting:{
        [postingId:string]:number[]
    }
}

const initialState:QuestionsState={
    questionsById:{},
    questionsByPosting:{}
}

export function questionsReducer (state:QuestionsState=initialState,action:QuestionActions):QuestionsState{

    return produce(state,state=>{
    switch (action.type){
        case "@@QUESTIONS/LOAD_QUESTIONS":
            {
                for (const question of action.questions){
                    state.questionsById[question.id] =question
                }
                state.questionsByPosting[action.postingId]=action.questions.map(question=>question.id)
            }
            break
        case "@@QUESTIONS/ADD_QUESTION":
            {
                state.questionsById[action.question.id] =action.question;
                if(state.questionsByPosting[action.question.posting_id]==null){
                    state.questionsByPosting[action.question.posting_id]= []
                }
                state.questionsByPosting[action.question.posting_id].push(action.question.id)
            }
            break
        case '@@QUESTIONS/UPLOAD_QUESTION':
            {
              
                state.questionsById[action.question.id]=action.question
            }
            break
    }
      
    })

}



