import React, { useState } from 'react'
import { View, Text, TouchableOpacity, Modal,StyleSheet,ImageBackground} from 'react-native'
import {WebView} from 'react-native-webview';
import { Actions } from 'react-native-router-flux';
import config from '../config';

let jsCode = `
    document.querySelector('.f1').submit()';
`;
// export default function PayPal() {
//     const [modal, SetModal] = useState(false)
//     const [status,setStatus]=useState('Pending')
//    const handleResponse: any=(data)=>{
//         if(data.title==='success'){
//             {SetModal(true),setStatus("Complete")}
//         }else if(data.title==='cancel'){
//             {SetModal(true),setStatus("Cancelled")}
//         }else{
//             return
//         }

//         }

//     return (
//         <View
//             style={{ marginTop: 100 }}>
//             <Modal
//                 visible={modal}
//                 onRequestClose={()=>SetModal(true)}
//             >

//                 <WebView
//                    source={{ uri: "http://localhost:3000" }}
//                    onNavigationStateChange={data =>
//                        handleResponse(data)
//                    }
//                    injectedJavaScript={`document.f1.submit()`}
//                     />
//             </Modal>
//             <TouchableOpacity
//                 style={{ width: 300, height: 100 }}
//                 onPress={()=>SetModal(true)}
//                 >
//                 <Text>Pay With PayPal</Text>
//             </TouchableOpacity>
//             <Text>
//                 Payment Status :{status}
//             </Text>
//         </View>





//     )
// }

export default class PayPal extends React.Component {
    state = {
        showModal: false,
        status: "Pending"
    };

    handleResponse = (data: { title: string; }) => {
        if (data.title === "success") {
            this.setState({ showModal: false, status: "Complete" });

        } else if (data.title === "cancel") {
            this.setState({ showModal: false, status: "Cancelled" });
        } else {
            return;
        }
    };
    render() {
        return (
            <View style={{ marginTop: 100 }}>

                <Modal
                    visible={this.state.showModal}
                    onRequestClose={() => this.setState({ showModal: false })}
                >
                    <WebView
                        source={{ uri: `https://hphour.xyz` }}
                        onNavigationStateChange={data =>
                            this.handleResponse(data)
                        }

                        injectedJavaScript={jsCode}
                        javaScriptEnabled={true}
                    />
                </Modal>
                <TouchableOpacity
                    style={{ width: 300, height: 100 }}
                    onPress={() => this.setState({ showModal: true })}
                >
                    <Text style={styles.pay}>想快啲有人JOIN？課金啦！</Text>
                </TouchableOpacity>
                <Text style={{textAlign:"center"}} >Payment Status: {this.state.status}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    pay: {

      justifyContent: 'center',
      alignItems: 'center',

      textAlign:"center",
      fontSize: 20,
      left:50
    },image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center"
      }
})