import React, { Component } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import styles from "./Datetime.style";



export default class EndTimePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isDateTimePickerVisible: false,
          selectedDate: "",
        }
    }

    

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });



  _handleDatePicked = date => {
    this.setState({ selectedDate: date.toString() });
    this._hideDateTimePicker();
  };

  render() {
    const { isDateTimePickerVisible, selectedDate } = this.state;
    
    return (
        
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={this._showDateTimePicker}>
          <View>
            <Text>選擇活動結束時間</Text>
          </View>
        </TouchableOpacity>

        <Text style={styles.text}>{selectedDate}</Text>
        <DateTimePicker
          isVisible={isDateTimePickerVisible}
          onConfirm={this._handleDatePicked}
          onCancel={this._hideDateTimePicker}
          mode="time"
        />
        {this.props.getEndTime(selectedDate)}
      </View>
    );
  }
}