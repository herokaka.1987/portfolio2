import React from 'react';
import {AccessToken, LoginManager} from "react-native-fbsdk"
import styles from './FbLogin.style'
import { Button, Icon } from 'native-base';

function FbLogin() {
    return (
       <Button dark style={styles.container} onPress={function handleFacebookLogin () {
            LoginManager.logInWithPermissions(['public_profile', 'email']).then(
                function (result) {
                    if (result.isCancelled) {
                    console.log('Login cancelled')
                    } else {
                        console.log('Login success with permissions: ' + result.grantedPermissions?.toString())
                        AccessToken.getCurrentAccessToken().then(
                            (data)=> {
                                console.log(data)
                                console.log(data?.accessToken.toString())
                            }
                        )
                    }
                },
                function (error) {
                    console.log('Login fail with error: ' + error)
                }
            )
        }}>
            <Icon name="logo-facebook" />
        </Button>
    )
}

export default FbLogin