import { ThunkDispatch } from '../store';
import { loadQuestions } from './action'

export function fetchQuestion(postingId: number) {
    return async (dispatch: ThunkDispatch) => {
        const res = await fetch(`https://hphour.xyz/Chat/questions?posting_id=${postingId}`)

        const json = await res.json();
        dispatch(loadQuestions(json, postingId))
    }
}