import { PlaceActions } from './action'

export interface GooglePlaceinfo {
    address: string;
    lat: number;
    lng: number;
}

export interface GooglePlaceinfoState {
    googlePlaceinfos: GooglePlaceinfo[],
    submitting: boolean,
}

const initialState: GooglePlaceinfoState = {
    googlePlaceinfos: [],
    submitting: false
}

export const placeinfoReducer = (oldState: GooglePlaceinfoState = initialState, action: PlaceActions): GooglePlaceinfoState => {
    switch (action.type) {
        case "@@PLACE/LOADED_PLACES":
            return {
                ...oldState,
                googlePlaceinfos: action.googlePlaceinfos
            }
        case "@@PLACE/SUBMITTING":
            return {
                ...oldState,
                submitting: action.isSubmitting
            }
    
        default:
            break;
    }
    return oldState;
}