import { GooglePlaceinfo } from "./reducers"

export function loadPlace(googlePlaceinfos: GooglePlaceinfo[]) {
    return {
        type: "@@PLACE/LOADED_PLACES" as "@@PLACE/LOADED_PLACES",
        googlePlaceinfos
    };
}

export function placeSubmitting(isSubmitting: boolean) {
    return {
        type: "@@PLACE/SUBMITTING" as "@@PLACE/SUBMITTING",
        isSubmitting
    }
}

export type PlaceActions = ReturnType<typeof loadPlace> | ReturnType<typeof placeSubmitting>;