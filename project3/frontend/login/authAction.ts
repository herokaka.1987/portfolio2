export function setJwtToken(token:string | null) {
    return {
        type:'@@SET_JWT_TOKEN' as '@@SET_JWT_TOKEN',
        token:token
    }
}

export function clearJwtToken() {
    return {
        type:'@@CLEAR_JWT_TOKEN' as '@@CLEAR_JWT_TOKEN',
    }
}

export type AuthActions = ReturnType<typeof setJwtToken> | ReturnType<typeof clearJwtToken>
 
