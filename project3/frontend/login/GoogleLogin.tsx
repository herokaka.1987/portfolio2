import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';

import {Icon,Button} from "native-base"
import {Actions, ActionConst} from "react-native-router-flux"
import {checkLoginGG} from "./action"
import {connect,useSelector} from "react-redux"
import { resolvePlugin } from '@babel/core';
import AsyncStorage from '@react-native-community/async-storage';
import {setJwtToken} from "./authAction"
import {RootState} from '../store'

interface Props {
  checkLoginGG: checkLoginGG;
  setJwtToken: setJwtToken;
  token:string
}


class Googlelogin extends React.Component<Props> {
  constructor(props: Readonly<Props>) {
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
    };
  }

  componentDidMount() {
    //initial configuration
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '241841771721-td6tij2q5a910f813n89vujepk2kj9ea.apps.googleusercontent.com',
    });
    
    //Check if user is already signed in
    this._isSignedIn();
  }

  _isSignedIn = async () => {
    
    const jwt_Token = await AsyncStorage.getItem('@jwt_Token')

    console.log(jwt_Token)

    if(jwt_Token == null) {

      console.log('googleLogin noSignedIn')
      Actions.replace('firstPage')


    }else if (await GoogleSignin.isSignedIn()) {
      //   // Alert.alert('User is already signed in');
      //   //Get the User details as user is already signed in
  
        const userInfo = await GoogleSignin.signIn();
        const detail = ({
          first_name:userInfo.user.givenName,
          icon:userInfo.user.photo,
          email:userInfo.user.email,
        })
  
        this.props.checkLoginGG(userInfo.user.givenName,userInfo.user.photo,userInfo.user.email)
        
<<<<<<< HEAD
        const res = await fetch('https://hphour.xyz/loginSucceed',{
=======
        const res = await fetch('http://localhost:8080/loginSucceed',{
>>>>>>> 09772d3b1e0b0455f470a7c2f650a595faff7480
            method:'POST',
            headers:{
              'Content-Type':'application/json'
            },
            body:JSON.stringify(detail)
          })
          
        const jwt_Token = await res.json()
  
        await AsyncStorage.setItem('@jwt_Token', jwt_Token.token)

        this.props.setJwtToken(jwt_Token.token)
  
        Actions.replace('secondPage', { signOutGoogle: this._signOut })
        // Actions.SecondPage({ type: ActionConst.REPLACE, signOutGoogle: this._signOut })
        // this._getCurrentUserInfo();
      } 
      else {
        //alert("Please Login");
        console.log('Please Login');
      }
      this.setState({ gettingLoginStatus: false });

    // const isSignedIn = await GoogleSignin.isSignedIn();
    
  };

  // _getCurrentUserInfo = async () => {
  //   try {
  //     const userInfo = await GoogleSignin.signInSilently();
  //     // console.log('User Info --> ', userInfo);
  //     this.setState({ userInfo: userInfo });
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_REQUIRED) {
  //       Alert.alert('User has not signed in yet');
  //       console.log('User has not signed in yet');
  //     } else {
  //       Alert.alert("Something went wrong. Unable to get user's info");
  //       console.log("Something went wrong. Unable to get user's info");
  //     }
  //   }
  // };

  _signIn = async () => {

    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();

      if(userInfo != null) {

        const detail = ({
          first_name:userInfo.user.givenName,
          icon:userInfo.user.photo,
          email:userInfo.user.email,
        })

        this.props.checkLoginGG(userInfo.user.givenName,userInfo.user.photo,userInfo.user.email)

<<<<<<< HEAD
        const res = await fetch('https://hphour.xyz/loginSucceed',{
=======
        const res = await fetch('http://localhost:8080/loginSucceed',{
>>>>>>> 09772d3b1e0b0455f470a7c2f650a595faff7480
          method:'POST',
          headers:{
            'Content-Type':'application/json'
          },
          body:JSON.stringify(detail)
        })
        console.log('post succeed')
        const jwt_Token = await res.json()

        console.log(`_signIn:${jwt_Token.token}`)

        await AsyncStorage.setItem('@jwt_Token', jwt_Token.token)

        this.props.setJwtToken(jwt_Token.token)

        Actions.replace('secondPage')
      }
      // console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log(error);
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };

  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null }); // Remove the user from your app's state as well
      console.log('logout succeed')
    } catch (error) {
      console.error(error);
    }
  };

  render() {
    //returning Loader untill we check for the already signed in user
    //@ts-ignore
    if (this.state.gettingLoginStatus) {
      return (
        <SafeAreaView style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </SafeAreaView>
      );
    } else {
      if (this.state.userInfo != null) {

        //Showing the User detail
        return (
          <SafeAreaView>
            {/* <Text style={styles.text}>Hello, {this.state.userInfo.user.name}{' '}</Text>  */}
            <Button dark onPress={this._signOut} style={styles.button}><Icon name="logo-google" /></Button>
            {/* <TouchableOpacity style={styles.button} onPress={this._signOut}> 
              <Text>Logout</Text> 
            </TouchableOpacity> */}
          </SafeAreaView>
        );
      } else {
        return (
          <SafeAreaView style={styles.container}>
            <Button dark onPress={this._signIn} style={styles.button}><Icon name="logo-google" /></Button>
          </SafeAreaView>
        );
      }
    }
  }
}

const styles = StyleSheet.create({
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: 50,
    height:50,
    borderRadius:50,
  },
  text: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});

export default connect((state: RootState) => {
  return {
    token: state.auth.token
  }
}, (dispatch) => {
  return {
    setJwtToken: (token:string | null) =>dispatch(setJwtToken(token)),
    checkLoginGG: (username: string, photo: string,email:string) => dispatch(checkLoginGG(username, photo,email))
  }
})(Googlelogin)