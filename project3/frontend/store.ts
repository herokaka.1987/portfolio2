import { createStore, combineReducers, compose, applyMiddleware } from 'redux'
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { FormDetailState, formReducer } from './form/reducer';
import { FormsActions } from './form/action';
// import { PlaceActions } from './GooglePlace/action'
import { AttendersActions } from './Attender/action';
// import { placeinfoReducer, GooglePlaceinfoState } from './GooglePlace/reducers'
// import { MeetingsState, meetingsReducer } from './meetings/reducer';
// import { QuestionsState, questionsReducer } from './questions/reducer';
import { loginReducer } from "./login/reducer"
import { LoginState } from "./login/reducer"
import { attenderReducer, AttendersState } from './Attender/reducer'
import { LoginActions } from './login/action';
import {AuthState} from './login/authReducer'
import {authReducer} from "./login/authReducer"
import { AuthActions } from './login/authAction';
import { QuestionsState, questionsReducer } from './question/reducer';
import { QuestionActions } from './question/action';


export type RootActions = FormsActions | AttendersActions | LoginActions | AuthActions | QuestionActions

export type ThunkDispatch = OldThunkDispatch<RootState, null, RootActions>


export interface RootState {
  // auth: AuthState,
  form: FormDetailState
  // place: GooglePlaceinfoState
  login:LoginState,
  attender: AttendersState
  auth:AuthState,
  // meetings: MeetingsState,
  questions: QuestionsState,
}

const reducer = combineReducers<RootState>({
  // auth: authReducer,
  form: formReducer,
  // place: placeinfoReducer,
  login:loginReducer,
  attender: attenderReducer,
  auth:authReducer,
  // meetings: meetingsReducer,
  questions: questionsReducer,
})
//@ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,
  composeEnhancers(
    applyMiddleware(thunk),
  ));



export interface RootState {

}
